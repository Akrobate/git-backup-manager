# git-backup-manager

Git backup manager is a tool to make a local backup of your remote git repositories.

* All the backups goes to backup folder
* The script creates a folder per git service

## Configuration

First of all, you must be able to clone and to pull the code from all remote repositories you plan to backup.

### First copy the configuration file

```bash
cp ./config/repositories.config.sh.example ./config/repositories.config.sh
```

### Cofiguration file zones

#### DEFALUT_BRANCHES

DEFALUT_BRANCHES are the branches that the script will try to backup. If branches does not exists, it just passes to the next one

```bash
declare -g DEFAULT_BRANCHES=(
"master"
"main"
"develop"
"release/candidate"
)
```

#### REPLACEMENTS

If you have a specific configuration in your .ssh/config you will be able to set the needed replacements here. Replacement syntax must be provided as from|to pattern

```bash
declare -g REPLACEMENTS=(
git@gitlab.com:Akrobate|git@Akrobate.gitlab.com:Akrobate
)
```

#### REPOSITORIES

The list of repositories to backup

```bash
declare -g REPOSITORIES=(
git@Akrobate.gitlab.com:Akrobate/physical-dashboard.git
)
```
