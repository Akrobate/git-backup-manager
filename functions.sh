loop(){
    local n=$#;
    local value=${!n};
    for(( i=1 ; i < $n ; i++))
    do
        ${value} "${!i}";
    done
}


processOne() {
    local repository_path=$1
    repository_path=$(applyAllPossibleRemplacements $repository_path)
    local current_path=$(pwd)
    cd ${current_path}/backup

    local git_provider_folder=$( generateServiceFolderNameFromGitPath $repository_path )
    if ! [ -d $git_provider_folder ]
    then
        echo "$git_provider_folder - does not exists."
        echo "creating"
        mkdir $git_provider_folder
    fi
    cd $git_provider_folder


    local folder=$( generateProjectFolderNameFromGitPath $repository_path )

    if ! [ -d $folder ]
    then
        echo "$folder - does not exists."
        echo "Cloning"
        git clone $repository_path
    fi

    echo "$folder exists."
    echo "Updating"
    cd $folder
    checkoutAndPullAlldefaultBranches

    cd $current_path
}


applyAllPossibleRemplacements() {
    local to_replace_path=$1

    for i in "${REPLACEMENTS[@]}"
    do
        to_replace_path=$(processOneRemplacement $i $to_replace_path)
    done

    echo $to_replace_path
}


checkoutAndPullAlldefaultBranches() {
    git fetch --all
    git pull --rebase
    local i=""
    for i in "${DEFAULT_BRANCHES[@]}"
    do
        local branch_exists=$(checkBranchExists $i)
        if [ $branch_exists -eq 1 ]
        then
            git checkout $i
            git pull --rebase origin $i
        fi
    done
}


processOneRemplacement() {
    local remplace_expression=$1
    local to_replace_string=$2

    local from=""
    local to=""
    IFS="|" read -r from to <<< $remplace_expression
    echo $(replace $to_replace_string $from $to)
}


checkBranchExists() {
    local branch_name=$1

    local maches_count=$(git branch -r | grep $branch_name | wc -l)
    if [ $maches_count -eq 0 ]
    then
        echo "0"
    else
        echo "1"
    fi
}


generateProjectFolderNameFromGitPath() {
    local regex="/(.[^/]*)\.git?"
    echo $(quickRegMatch $regex $1)
}


generateServiceFolderNameFromGitPath() {
    local repository_path=$1
    repository_path=$(toLowerCase $repository_path)

    local regex="@(.[^/]*)/"
    local match_1=$(quickRegMatch $regex $repository_path);
    echo $(replace $match_1 ':' '.');
}


replace() {
    local str=$1
    local from=$2
    local to=$3
    echo ${str} | sed "s|${from}|${to}|g"
}


toLowerCase() {
    echo $1 | tr '[:upper:]' '[:lower:]'
}


quickRegMatch() {
    local regex=$1
    local str=$2
    local match_1="";
    if [[ $str =~ $regex ]]
    then
        match_1="${BASH_REMATCH[1]}"
    fi
    echo $match_1;
}